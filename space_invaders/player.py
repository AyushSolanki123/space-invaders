from space_invaders.constants import PLAYER_IMAGE

class Player:
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 13

    def draw(self, win):
        win.blit(PLAYER_IMAGE, (self.x, self.y))
