import math
from space_invaders.constants import BULLET_IMAGE

class Projectile:
    def __init__(self, enemy, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 30
        self.state = 'ready'

    def draw(self, win):        
        win.blit(BULLET_IMAGE, (self.x + 16, self.y + 10))
        self.state = 'fire'

    def hit(self, enemy):
        distance = math.sqrt(
            ((enemy.hitbox[0] - self.x) ** 2) + ((enemy.hitbox[1] - self.y) ** 2))
        if distance < 27:
            return True
        else:
            return False
